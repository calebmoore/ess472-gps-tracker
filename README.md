# GPS TRACKER #

This repository contains the source code for both the GPS Tracker module and the C# Windows Tracking app built for ESS 472.

### How do I get set up? ###

* Download the latest version (Check the Downloads section on the left).
* Program the tracker with the Arduino sketch.
* Run the app and start tracking!

### Who do I talk to? ###

* Caleb Moore: calebm12@uw.edu
