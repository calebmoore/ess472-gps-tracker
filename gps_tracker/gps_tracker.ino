#include <SoftwareSerial.h>

#include <TinyGPS.h>

TinyGPS gps;
SoftwareSerial ss(3, 4); // RX 3, TX 4

// random id for unique identification
int id;

void setup()
{
  Serial.begin(9600);
  ss.begin(4800);
  
  pinMode(A0, INPUT);
  randomSeed(analogRead(A0));
  id = random(0x7fff);
}

void loop()
{
  bool newData = false;
  unsigned long chars;
  unsigned short sentences, failed;
  
  // For one second we parse GPS data and report some key values
  for (unsigned long start = millis(); millis() - start < 1000;)
  {
    while (ss.available())
    {
      char c = ss.read();
      // Serial.write(c); // uncomment this line if you want to see the GPS data flowing
      if (gps.encode(c)) // Did a new valid sentence come in?
        newData = true;
    }
  }
  
  Serial.print(id, HEX);
  Serial.print("~");

  if (newData)
  {
    float flat, flon, falt, fvel, fcrs;
    unsigned long age;
    gps.f_get_position(&flat, &flon, &age);
    falt = gps.f_altitude();
    fvel = gps.f_speed_mps();
    fcrs = gps.f_course();
    Serial.print("LAT=");
    Serial.print(flat == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flat, 6);
    Serial.print(" LON=");
    Serial.print(flon == TinyGPS::GPS_INVALID_F_ANGLE ? 0.0 : flon, 6);
    Serial.print(" AGE=");
    Serial.print(age == TinyGPS::GPS_INVALID_AGE ? 0.0 : age, DEC);
    Serial.print(" ALT=");
    Serial.print(falt == TinyGPS::GPS_INVALID_F_ALTITUDE ? 0.0 : falt, 6);
    Serial.print(" SPD=");
    Serial.print(fvel == TinyGPS::GPS_INVALID_F_SPEED ? 0.0 : fvel, 6);
    Serial.print(" CRS=");
    Serial.print(fcrs == TinyGPS::GPS_INVALID_F_ANGLE ? "X" : TinyGPS::cardinal(fcrs));
    Serial.print(" SAT=");
    Serial.print(gps.satellites() == TinyGPS::GPS_INVALID_SATELLITES ? 0 : gps.satellites());
    Serial.print(" PREC=");
    Serial.print(gps.hdop() == TinyGPS::GPS_INVALID_HDOP ? 0 : gps.hdop());
  }
  
  gps.stats(&chars, &sentences, &failed);
  Serial.print(" CHARS=");
  Serial.print(chars);
  Serial.print(" SENTENCES=");
  Serial.print(sentences);
  Serial.print(" ERR=");
  Serial.println(failed);
  if (chars == 0)
    Serial.println("** No characters received from GPS: check wiring **");
}


